#include <iostream>
#include <vector>
#include <memory.h>

using namespace std;

class Test {
private:
    static const int SIZE=100;
    int *memoria;
public:
    Test(){
        cout << "constructor" << endl;
        memoria = new int[SIZE]{};
    }
    Test(int i){
        cout << "constructor parametrizado" << endl;
        memoria = new int[SIZE]{};
        for(int i=0; i<SIZE; i++) {
            memoria[i] = 7*i;
        }
    }
    Test(const Test &otro){
        cout << "constructor de copia" << endl;
        memoria = new int[SIZE]{};
        memcpy(memoria, otro.memoria, SIZE*sizeof(int));
    }
    Test &operator=(const Test &otro){
        cout << "asignación" << endl;
        memoria = new int[SIZE]{};
        memcpy(memoria, otro.memoria, SIZE*sizeof(int));
        return *this;
    }
    ~Test(){
        cout << "destructor" << endl;
        delete[] memoria;
    }
    friend ostream &operator<<(ostream &out, const Test &test);
};

ostream &operator<<(ostream &out, const Test &test){
    out << "hola";
    return out;
}

Test getTest(){
    return Test();
}

int main() {
    Test t1 = getTest();
    cout << t1 << endl;
    vector<Test> vec;
    vec.push_back(Test());
   
    Test &rTest1 = t1;

    //Test &rTest2 = getTest();

    const Test &rTest2 = getTest();

    Test test2(Test(1));

    return 0;
}