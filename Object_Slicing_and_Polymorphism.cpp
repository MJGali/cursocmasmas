#include <iostream>
using namespace std;

class Padre {
	private:
		int uno;
	public:
		Padre(): uno(0){}
		Padre(const Padre &otro): uno(0) {
			uno = otro.uno;
			cout << "copia padre" << endl;
		}
		virtual void print(){
			cout << "padre" << endl;
		}
		virtual ~Padre() {}
};

class Hijo : public Padre{
	private:
		int dos;
	public:
		Hijo(): dos(0) {};
		void print(){
			cout << "hijo" << endl;
		}
};

int main() {
	Hijo h1;
	Padre &p1 = h1;
	p1.print();
	Padre p2 = Hijo();
	p2.print();
	return 0;
}