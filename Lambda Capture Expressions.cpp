#include <iostream>
using namespace std;

int main () {

	int uno = 1;
	int dos = 2;
	int tres = 3;
	
	[uno, dos](){cout << uno << ", " << dos << endl; }();
	
	[=](){cout << uno << ", " << dos << ", " << tres << endl; } ();
	
	[=, &tres](){tres = 4; cout << uno << ", " << dos << ", ";}();
	 cout << tres << endl; 
	 
	[&, dos, tres](){uno = 8; cout << uno << ", " << dos << ", ";}();
	 cout << tres << endl; 
	
	
	return 0;
}