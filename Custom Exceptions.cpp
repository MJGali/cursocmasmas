#include <iostream>
#include <exception>
using namespace std;


class Excepcion: public exception {
public:
    virtual const char* what() const throw(){
        return "akajdbad";
    
    }    
};

class Test{
public: 
    void VaMal(){
        throw Excepcion();
    }
};


int main(){
    
    Test test;
    
    try{
        test.VaMal();
    }
    catch(Excepcion &e) {
            cout << e.what() << endl;
    }
    return 0;
    
}