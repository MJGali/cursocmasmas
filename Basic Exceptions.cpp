#include <iostream>
using namespace std;

void GestorError(){
    
    bool error1 = false;
    bool error2 = false;
    bool error3 = true;
    
    if (error1 == true) {
        throw "no tira";
    }    
    if (error2) {
        throw string("tampoco va");
    }
    if (error3) {
        throw 4;
    }
}

void UsaGestor(){
   GestorError();    
}

int main(){
    try{
        UsaGestor();
    }
    catch(int e) {
        cout << "Fallo tipo: " << e << endl;
    }
    catch(char const *e){
        cout << "Error, " << e << endl;
    }
    catch (string &e){
        cout << "Otro error, " << e << endl;
    }
    cout << "Funcionando" << endl;

    return 0;
    
}