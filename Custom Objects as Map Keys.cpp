#include <iostream>
#include <map>
using namespace std;

class Persona {
	private:
		string nombre;
		int edad;
		
	public:
		Persona(): nombre(""), edad(0) {}
		Persona(string nombre, int edad) : nombre(nombre), edad(edad) {}
		void print() const {
				cout << nombre << ": " << edad << endl;
		}
		bool operator<(const Persona &otra) const{
			return nombre < otra.nombre;
		}
};

int main() {
	map<Persona, int> gente;
	
	gente[Persona("pipo",  14)] = 14;
	gente[Persona("pipe",  5)] = 244;
	gente[Persona("popi",  76)] = 76;

	for (map<Persona, int>::iterator i = gente.begin(); i != gente.end(); i++){
		cout << i->second << " - ";
		i->first.print();
	}
	return 0;
}
