#ifndef RING_H
#define RING_H

#include <iostream>
using namespace std;

template<class T>
class ring
{
private:
    int m_pos;
    int m_tamano;
    T *m_valor;
public:
    class iterator;
public:
    ring(int tamano):m_pos(0), m_tamano(tamano), m_valor(NULL){
        m_valor = new T[tamano];
    }
    ~ring(){
        delete[] m_valor;
    }

    int size() {
        return m_tamano;
    }

    iterator begin(){
        return iterator(0, *this);
    }

    iterator end(){
        return iterator(m_tamano, *this);
    }

    void add(T valor){
        m_valor[m_pos] = valor;
        m_pos++;
        if (m_pos == m_tamano){
            m_pos = 0;
        }
    }
    T &get(int pos){
        return m_valor[pos];
    }
};

template<class T>
class ring<T>::iterator
{
private:
    int m_pos;
    ring m_ring;
public:
    iterator(int pos, ring &aRing):m_pos(pos), m_ring(aRing) {

    }

    iterator &operator++(int){
        m_pos++;

        return  *this;
    }

    T &operator*(){
        return m_ring.get(m_pos);
    }
};


#endif // RING_H
