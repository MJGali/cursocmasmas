#include <iostream>
#include <map>
using namespace std;


int main() {

	multimap<int, string> MM;
	
	MM.insert(make_pair(30, "pipo"));
	MM.insert(make_pair(20, "popi"));
	MM.insert(make_pair(10, "pepe"));
	MM.insert(make_pair(40, "pipa"));
	
	for (multimap<int, string>::iterator i=MM.begin(); i != MM.end(); i++) {
		cout << i->first << ": " << i->second << endl;
	}
	
	for (multimap<int, string>::iterator i=MM.find(10); i != MM.end(); i++) {
		cout << i->first << ": " << i->second << endl;
	}
	
	return 0;
}