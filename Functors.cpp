#include <iostream>
using namespace std;

struct Test{
	virtual bool operator()(string &texto)=0;
	virtual ~Test(){};
};

struct MatchTest: public Test{
	virtual bool operator()(string &texto){
		return texto == "leon";
	}
};

void check(string texto, Test &test) {
	if(test(texto)) {
		cout << "texto ok" << endl;
	}
	else {	
		cout << "texto no va" << endl;
	}
}

int main() {
	MatchTest pred;
	MatchTest m;
	string value = "leon";
	check("leon", m);
	return 0;
}