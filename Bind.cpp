#include <iostream>
#include <functional>
using namespace std;
using namespace placeholders;

int suma(int a, int b, int c){
    cout << a << ", " << b << ", " << c << endl;
    return a + b + c;
}

void run(function<int(int, int)> func){
    func(7, 3);
}

int run2(function<int(int, int)> func2){
    func2(5, 5);
}

int main() {

    //cout << suma(1,2,3) << endl;
    auto calcula1 = bind(suma, _1, _2, _3);
    cout << calcula1(10,2, 6) << endl;

    auto calcula2 = bind(suma, _1, 200, _2);
    cout << calcula2(10,20) << endl;

    run(calcula2);
    cout << run2(calcula2) << endl;

    return 0;
}