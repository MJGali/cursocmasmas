#include <iostream>
using namespace std;

template<class T>
void print(T n){
	cout << n << endl;
}

int main() {
	print<string>("hola");	
	print<int>(5);
	return 0;
}