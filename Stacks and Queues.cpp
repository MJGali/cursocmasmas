#include <iostream>
#include <stack>
#include <queue>
using namespace std;

class Prueba {
    private:
        string nombre;
    public:
        Prueba(string nombre) : 
        nombre (nombre) {
        }
            
        ~Prueba() {
        }
        void imprime(){
            cout << nombre << endl;
        }
};

int main(){
    //stack<Prueba> prueba;
    queue<Prueba> prueba;
    
    prueba.push(Prueba("pipo"));
    prueba.push(Prueba("pipi"));
    prueba.push(Prueba("popi"));
    
    prueba.back().imprime();
    
    /*Prueba p1 = prueba.top();
    p1.imprime();
    
    prueba.pop();
    Prueba p2 = prueba.top();
    p2.imprime();*/
    
    while(prueba.size()!=0){
        //Prueba &p1 = prueba.top();
        Prueba &p1 = prueba.front();
        p1.imprime();
        prueba.pop();
    }
    
    
    return 0;
}