#include <iostream>
#include <vector>
#include <memory.h>

using namespace std;

class Test {
private:
    static const int SIZE=100;
    int *memoria{nullptr};
public:
    Test(){
        memoria = new int[SIZE]{};
    }
    Test(int i){
        memoria = new int[SIZE]{};
        for(int i=0; i<SIZE; i++) {
            memoria[i] = 7*i;
        }
    }
    Test(const Test &otro){
        memoria = new int[SIZE]{};
        memcpy(memoria, otro.memoria, SIZE*sizeof(int));
    }

    Test(Test &&otro){
        cout << "Constructor" << endl;
        memoria = otro.memoria;
        otro.memoria = nullptr;
    }

    Test &operator=(const Test &otro){
        memoria = new int[SIZE]{};
        memcpy(memoria, otro.memoria, SIZE*sizeof(int));
        return *this;
    }

    Test &operator=(Test &&otro){
        cout << "Asignacion" << endl;
        delete [] memoria;
        memoria = otro.memoria;
        otro.memoria = nullptr;

        return *this;

    }
    ~Test(){
        delete[] memoria;
    }
    friend ostream &operator<<(ostream &out, const Test &test);
};

ostream &operator<<(ostream &out, const Test &test){
    out << "hola";
    return out;
}

Test getTest(){
    return Test();
}

int main() {

    vector<Test> vec;
    vec.push_back(Test());

    Test test;
    test = getTest();

    
    return 0;
}