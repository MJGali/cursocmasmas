#include <iostream>
using namespace std;

class Animal {
	public:
		virtual void corre() = 0;
		virtual void habla() = 0;
};

class Perro: public Animal {
	public:
		virtual void habla() {
			cout << "guau" << endl;
		}	
};

class Labrador: public Perro {
	public:
		Labrador() {
			cout << "nuevo labrador" << endl;
		}
		virtual void corre() {	
			cout << "Labrador corre" << endl;
		}
};

void test(Animal &a) {	
	a.corre();	
}

int main() {
	Labrador lab;
	lab.corre();
	lab.habla();
	Animal *animales[5];
	animales[0] = &lab;
	animales[0]->habla();
	
	test(lab);
	
	return 0;
}