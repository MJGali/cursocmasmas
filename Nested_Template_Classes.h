#ifndef RING_H
#define RING_H

#include <iostream>
using namespace std;

template<class T>
class ring
{
    public:
        class iterator;
};

template<class T>
class ring<T>::iterator
{
    public:
        void print()
        {
            cout << "Iterador" << endl;
        }
};

#endif //RING_H