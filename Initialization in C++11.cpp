#include <iostream>
#include <vector>
using namespace std;

int main(){
	int valores{5};	
	cout << valores << endl;
		
	string texto{"Hola"};
	cout << texto << endl;
	
	int numeros[]{1,2,3};
	cout << numeros[1] << endl;
	
	int *pInts = new int[3]{1,2,3};
	cout << pInts[1] << endl;
	delete pInts;
	
	int valores1{};	// devuelve 0
	cout << valores1 << endl;
	
	int *pAlgo{};  // como asignarlo a nullptr
	cout << *pAlgo << endl;
	
	int numeros2[5]{};
	cout << numeros2[1] << endl;
	
	struct {
		int valor;
		string texto;
	} s1 = {5, "hola"};
	
	cout << s1.texto << endl;
	
	vector<string> cadenas{"uno", "dos", "tres"};
	cout << cadenas[2] << endl;
	
	return 0;
}