#include <iostream>
using namespace std;
 
template <class T>
auto test(T valor)  -> decltype(valor) {
	return valor;
}

template <class S, class U>
auto prueba(S uno, U dos) -> decltype(uno + dos) {
    return uno + dos;
}

int main() {

	auto valor = 5;
	auto texto = "pipo";
	
	cout << valor << endl;
	cout << test(texto) << endl;
    cout << prueba(1, 2) << endl;
	
	return 0;
}