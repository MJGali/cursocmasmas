#include <iostream>

using namespace std;

#pragma pack(push, 1)

struct person {
    char name[50];
    int age;
    double weight;
};

#pragma pack(pop)

int main(){

    cout << sizeof(person) << endl;
    return 0;
    
}