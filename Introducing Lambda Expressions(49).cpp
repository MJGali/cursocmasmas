#include <iostream>
using namespace std;

void Test(void (*funcion)()) {
	funcion();
}


int main () {
	
	auto func = [](){cout << "hola" << endl;};
	
	func();
	Test(func);
	Test([](){cout << "hola" << endl;});
	
	return 0;
}