#include <iostream>
using namespace std;

class Padre{
public:
    virtual void speak(){
        cout << "Padre!" << endl;
    }

};

class Hermano: public Padre{

};

class Hermana: public Padre {

};

int main() {

    Padre padre;
    Hermano hermano;
    Hermana hermana;

    Padre *pp = &hermano;

    Hermana *phoho = reinterpret_cast<Hermana *>(pp);

    if (phoho == nullptr) {
        cout << "Casteo inválido" << endl;
    } else {
        cout << phoho << endl;
    }

    return 0;
}