#include <iostream>
#include <vector>
#include <map>
using namespace std;


int main()
{
  map<string, vector<int> > prueba;
  
  prueba["pipo"].push_back(19);
  prueba["popi"].push_back(49);
  prueba["popo"].push_back(39);
  
  for (map<string, vector<int> >::iterator i = prueba.begin(); i != prueba.end(); i++){
      string nombre = i->first;
      vector<int> lista = i->second;
      
      cout << nombre << ": " << flush;
      
      for (int i = 0; i < lista.size(); i++) {
          cout << lista[i] << " " << flush;
      }
      
      cout << endl;      
  }

  return 0;
}
