#include <iostream>
#include <vector>
#include <initializer_list>
using namespace std;

class Prueba {
	public:
		Prueba(initializer_list<string> cadenas){
			for(auto valor: cadenas) {
					cout << valor << endl;
			}
		}
		void print(initializer_list<string> cadenas){
			 for(auto valor: cadenas) {
					cout << valor << endl;
			}
		}
};

int main(){

	vector<int> numeros {1,2,3,4};
	cout << numeros[2] << endl;
	
	Prueba prueba{"pipo", "pipi", "pepe"};
	prueba.print({"lilo", "lulu", "loli"});
	
	return 0;
}