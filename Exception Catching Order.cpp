#include <iostream>
#include <exception>
using namespace std;

void excepcion(){
    bool error1 = false;
    bool error2 = true;
    
    if (error1) 
        throw bad_alloc();
    if (error2)
        throw exception();
}

int main(){
    
    try {
        excepcion();
    }
    catch(exception &e){
        cout << e.what() << endl;
    }
    catch(bad_alloc &e){
        cout << e.what() << endl;
    }    
    return 0;
    
}