#include <iostream>
#include <set>
using namespace std;

class Prueba {
	int i;
	string nombre;
	
	public:
	    Prueba(): 
	        i(0), nombre("") {
	    }
		Prueba(int i, string nombre) : 
		    i(i), nombre(nombre) {
	    }
		void imprime() const {
		    cout << i << ": " << nombre << endl;
		}
		bool operator<(const Prueba &otra) const {
		    return nombre < otra.nombre;
		}
		
};

int main() {

	set<int> numeros;
	
	numeros.insert(10);
	numeros.insert(20);
	numeros.insert(30);
	numeros.insert(40);
	numeros.insert(40);
	
	for (set<int>::iterator i = numeros.begin(); i != numeros.end(); i++){
		cout << *i << endl;
	}
	
	set<int>::iterator encontrado = numeros.find(30);
	
	if (encontrado != numeros.end()){
		cout << "Encontrado: " << *encontrado << endl;
	}
	
	set<Prueba> pruebas;
	
	pruebas.insert(Prueba(10, "pipo"));
	pruebas.insert(Prueba(40, "pipi"));
	pruebas.insert(Prueba(50, "popi"));
	
	for (set<Prueba>::iterator i = pruebas.begin(); i != pruebas.end(); i++){
		pruebas->imprime();
	}
	
	return 0;
}