#include <iostream>
#include <vector>
using namespace std;

int main(){
	int valores[] = {1, 2, 3};
	
	cout << valores[0] << endl;
	
	class C {
		public:
			string texto;
			int id;
	};
	
	C c1 = {"hola", 1};
	
	cout << c1.texto << endl;
	
	struct S{
		string texto;
		int id;
	};
	
	S s1 = {"hola", 1};
	
	cout << s1.texto << endl;
	
	struct R{
		string texto;
		int id;
	}r1 = {"hola", 1}, r2 = {"quehay",2};
	
	cout << r1.texto << endl;
	cout << r2.texto << endl;
	
	vector<string> cadenas;
	
	cadenas.push_back("uno");
	cadenas.push_back("dos");
	cadenas.push_back("tres");
	
	cout << cadenas[0] << endl;
	
	
	
	return 0;
}