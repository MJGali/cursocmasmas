#include <iostream>
#include <memory>
using namespace std;

class Test {
public: 
    Test() {
        cout << "creado" << endl;
    }
    void greet(){
        cout << "hola" << endl;
    }
    ~Test(){
        cout << "destruido" << endl;
    }
};

int main() {

    //shared_ptr<Test> pTest1(new Test());
   shared_ptr<Test> pTest2(nullptr);
   {
        shared_ptr<Test> pTest1 = make_shared<Test>();
        pTest2 = pTest1;
        auto pTest3 = pTest1;
   }

    cout << "Terminado" << endl;

    return 0;
