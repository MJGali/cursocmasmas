#include <iostream>
using namespace std;

template<class T>
void print(T n){
	cout << "Template" << n << endl;
}

void print(int valor){
	cout << "NO template" << valor << endl;
}

template<class T>
void show(){
	cout << T() << endl;
}

int main() {
	print<string>("hola");	
	print<int>(5);
	print("Hola");
	print(3);
	show<double>();
	return 0;
}