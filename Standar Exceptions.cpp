#include <iostream>
using namespace std;


class GestorError {
    public:
        GestorError() {
            char *pMemory = new char[999999999];
            delete [] pMemory;
        }
};


int main(){
    
    try {
        GestorError error;
    }
    catch(bad_alloc &e) {
        cout << e.what() << endl;
    }

    return 0;
    
}