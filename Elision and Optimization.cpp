#include <iostream>
#include <vector>
using namespace std;

class Test {
public:
    Test(){
        cout << "constructor" << endl;
    }
    Test(int i){
        cout << "constructor parametrizado" << endl;
    }
    Test &operator=(const Test &otro){
        cout << "asignación" << endl;
        return *this;
    }
    ~Test(){
        cout << "destructor" << endl;
    }
    friend ostream &operator<<(ostream &out, const Test &test);
};

ostream &operator<<(ostream &out, const Test &test){
    out << "hola";
    return out;
}

Test getTest(){
    return Test();
}

int main() {
    Test t1 = getTest();
    cout << t1 << endl;
    return 0;
}