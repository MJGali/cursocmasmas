#include <iostream>
#include "complejo.h"

using namespace std;
using namespace cursocmasmas;

int main(){
	
	complejo c1(3,4);
	complejo c2(2,3);
	
	if (c1 == c2) {
		cout << "Iguales" << endl;
	}	
	else {
		cout << "Desiguales" << endl;
	}
	return 0;
	
}