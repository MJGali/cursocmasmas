#include <iostream>
#include <memory>

using namespace std;

class Test {
public: 
    Test() {
        cout << "creado" << endl;
    }
    void greet(){
        cout << "hola" << endl;
    }
    ~Test(){
        cout << "destruido" << endl;
    }
};

class Temp {
private:
    unique_ptr<Test[]> pTest;

public:
    Temp(): pTest(new Test[2]){

    }
};

int main() {

    Temp temp;



    //unique_ptr<Test> pTest(new Test);
    //pTest->greet();    
    cout << "Finished1" << endl;


    return 0;
}