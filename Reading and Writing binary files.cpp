#include <iostream>
#include <fstream>

using namespace std;

#pragma pack(push, 1)
 
struct Persona {
		char  nombre[50];
		int edad;
		double peso;
};

int main() {
	
	Persona alguien = {"pepe", 22,  64.7};
	
	string fichero = "test.bin";
	ofstream ficheroSalida;
	
	ficheroSalida.open(fichero, ios::binary);
	
	if (ficheroSalida.is_open()) {
		ficheroSalida.write(reinterpret_cast<char *>(&alguien), sizeof(Persona));
		ficheroSalida.close();
	}
	else {
		cout << "No se puede crear " + fichero << endl;	
	}
	
	Persona otra = {};
	ifstream ficheroEntrada;
	ficheroEntrada.open(fichero, ios::binary);
	
	if (ficheroEntrada.is_open()){
		ficheroEntrada.read(reinterpret_cast<char *>(&otra), sizeof(Persona));
		ficheroEntrada.close();
	}
	else {
		cout << "No se puede leer " + fichero << endl;	
	}
	
	cout << otra.nombre << ", " << otra.edad << ",  " << otra.peso << endl;
		
	return 0;
}