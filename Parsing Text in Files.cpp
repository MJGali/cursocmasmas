#include <iostream>
#include <fstream>
using namespace std;

int main(){
    
    string nombre = "texto.txt";
    ifstream fichero;
    
    fichero.open(nombre);
    
    if (!fichero.is_open())
        return 1;
        
    while(fichero){
        string cadena;
        getline(fichero, cadena, ':');
        int poblacion;
        fichero >> poblacion;
        cout << cadena << "--" << poblacion << endl;        
    }
        
    fichero.close();

    return 0;
    
}