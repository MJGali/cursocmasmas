#include <iostream>
#include <fstream>
using namespace std;

int main(){
    
    ifstream infile;
    
    string inputFileName = "text.txt";
    
    infile.open(inputFileName);
    
    if (infile.is_open()){
        string linea;
        
        while (!infile.eof()){
            getline(infile, linea);
            cout << linea << endl;
        }
        infile.close();       
    }
    else{
        cout << "No se pudo abrir el fichero: " << inputFileName << endl;
    }

    return 0;
    
}