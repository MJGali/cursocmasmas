#include "complejo.h"

namespace cursocmasmas {
	
	complejo::complejo(): real(0), imaginaria(0){}
	
	complejo::complejo(double real, double imaginaria): real (real), imaginaria(imaginaria){}
	
	complejo::complejo(const complejo &otro) {
		real = otro.real;
		imaginaria = otra.imaginaria;		
	}
	
	const complejo &complejo::operator=(const complejo &otro){
		real = otro.real;
		imaginaria = otro.imaginaria;
		
		return *this;
	}
	
	ostream &operator<<(ostream &salida, complejo &c){
		out << "(" << c.parteReal() << ", " << c.parteImaginaria() << ")";
		return out;		
	}
	
}