#include <iostream>
#include <map>
using namespace std;

class Persona {
	private:
		string nombre;
		int edad;
		
	public:
		Persona(): nombre(""), edad(0) {}
		Persona(const Persona &otra) { nombre = otra.nombre; edad = otra.edad; cout << "copia" << endl; }
		Persona(string nombre, int edad) : nombre(nombre), edad(edad) {}
		void print() const {
				cout << nombre << ": " << edad << endl;
		}
};

int main() {
	map<int, Persona> gente;
	
	gente[0] = Persona("pipo",  14);
	gente[1] = Persona("pipe",  23);
	gente[2] = Persona("popi",  76);
	
	gente.insert(make_pair(55, Persona("pupi", 34)));
    gente.insert(make_pair(35, Persona("pape", 44)));
    
	for (map<int, Persona>::iterator i = gente.begin(); i != gente.end(); i++){
		cout << i->first << " " << endl;
		i->second.print();
	}
	return 0;
}
