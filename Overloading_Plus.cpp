#include <iostream>
#include "complejo.h"

using namespace std;
using namespace cursocmasmas;

int main(){
	
	complejo c1(3,4);
	complejo c2(2,3);
	complejo c3(4,3);
	
	cout << c1 << endl;
	cout << c1 + c2 << endl;
	
	complejo c5 = c3 + 1;
	
	cout << c5 << endl;
	
	return 0;
	
}