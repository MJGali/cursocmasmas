#include <iostream>
#include <vector>

using namespace std;

int main(){

    vector<double> numeros(20);

    cout << "Tamaño: " << numeros.size() << endl;

    int capacidad = numeros.capacity();

    cout << "Capacidad: " << capacidad << endl;
    
    for (int i = 0; i < 10000; i++)
    {
        if(numeros.capacity() != capacidad) 
	{
	    capacidad = numeros.capacity();
            cout << "Capacidad: " << capacidad << endl;
	}
	numeros.push_back(i);
    }

    numeros.clear();
    cout << numeros[2] << endl;
    cout << "Tamaño: " << numeros.size() << endl;
    cout << "Capacidad: " << numeros.capacity() << endl;

    return 0;
    
}