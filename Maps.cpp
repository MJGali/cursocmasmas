#include <iostream>
#include <map>
using namespace std;

int main() {
	map<string, int> edades;
	
	edades["pipo"] = 14;
	edades["pipe"] = 23;
	edades["popi"] = 76;
	
	edades.insert(pair<string, int>("pupo", 53));
	for (map<string, int>::iterator i = edades.begin(); i != edades.end(); i++) {	
		pair<string, int> edad = *i;
		
		cout << edad.first << ": " << edad.second << endl;
	}
	
	for (map<string, int>::iterator i = edades.begin(); i != edades.end(); i++) {	
		cout << i->first << " :" << i->second << endl;
 	}
	
	if (edades.find("pipo") != edades.end()) {
		cout << "pipo encontrado";
	}
	else {
		cout << "pipo NO encontrado";
	}
	
	return 0;
}