#include <iostream>
using namespace std;

class Padre{
public:
    void speak(){
        cout << "Padre!" << endl;
    }

};

class Hermano: public Padre{

};

class Hermana: public Padre {

};

int main() {

    Padre padre;
    Hermano hermano;

    float valor = 3.23;
    cout << static_cast<int>(valor) << endl;

    Hermano *pho = static_cast<Hermano *>(&padre);

    cout << pho << endl;

    Padre *pp = &hermano;

    Hermano *phoho = static_cast<Hermano *>(pp);

    cout << pp << endl;

    Padre &&p = static_cast<Padre &&>(padre);
    
    return 0;
}