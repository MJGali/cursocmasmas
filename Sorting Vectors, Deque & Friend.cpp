#include <iostream>
#include <vector>
using namespace std;

class Prueba {
    int id;
    string nombre;
    public:
        Prueba(int id, string nombre): id(id), nombre(nombre) {}
        void imprime() {
            cout << id << ": " << nombre << endl;
        }
        //bool operator<(const Prueba& otra) const {
        //    return nombre < otra.nombre;
        //}
        friend bool comp(const Prueba &a, const Prueba &b);
};

bool comp(const Prueba &a, const Prueba &b) {
    return a.nombre < b.nombre;
}

int main()
{
  vector<Prueba> prueba;
  
  prueba.push_back(Prueba(10, "pipo"));
  prueba.push_back(Prueba(20, "popi"));
  prueba.push_back(Prueba(40, "pipi"));
  prueba.push_back(Prueba(30, "popo"));
  
  sort(prueba.begin(), prueba.end(), comp);
  
  for (int i = 0; i < prueba.size(); i++) {
      prueba[i].imprime();
  }
  
  return 0;
}