#include <iostream>
using namespace std;

class Prueba {
    private: 
        int id;
        string nombre;
    public:
        Prueba(): id(0), nombre("") {
        }
        Prueba(int id, string nombre): id(id), nombre(nombre) {
        }
        void imprime(){
            cout << id << ": " << nombre << endl;
        }
        const Prueba &operator=(const Prueba &otro){
            cout << "Asignación" << endl;
            
            id = otro.id;
            nombre = otro.nombre;
            
            
            return *this;
        }
};

int main(){
        
    Prueba test1(2, "pipo");
    cout << "Test1 " << flush;
    test1.imprime();
    Prueba test2(3, "popi");
    cout << "Test2 " << flush;
    test2.imprime();
    //Prueba test3(5, "pipi");
    //test3.imprime();
    Prueba test3;
    test3.operator=(test2);
    test3.imprime();
    
    
    return 0;
}
