#ifndef COMPLEJO
#define COMPLEJO

#include <iostream>
using namespace std;

namespace cursocmasmas {
	
	class complejo{
	private :
		double real;
		double imaginaria;
		
	public:
		complejo();
		complejo(double real, double imaginaria);
		complejo(const complejo &otro);
		const complejo &operator=(const complejo &otro);
		double parteReal(){return real;}
		double parteImaginaria(){return imaginaria;}
	};
	
	ostream &operator<<(ostream &salida, const complejo &c);
	
}

 #endif