#include <iostream>
#include <A_Ring_Buffer_Class.h>

using namespace std;

int main()
{

    ring<string> cadenas(3);

    cadenas.add("uno");
    cadenas.add("dos");
    cadenas.add("tres");
    cadenas.add("cuatro");

    for(int i=0; i<cadenas.tamano(); i++){
        cout << cadenas.get(i) << endl;
    }
    return 0;
}