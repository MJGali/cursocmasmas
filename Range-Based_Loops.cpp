#include <iostream>
#include <vector>
using namespace std;

int main() {
	
	auto cadenas = {"uno", "dos", "tres"};
	
	for(auto cadena: cadenas){
		cout << cadena << endl;
	}
	
	vector<int> numeros;
	
	numeros.push_back(1);
	numeros.push_back(3);
	numeros.push_back(5);
	numeros.push_back(6);
	
	for (auto numero: numeros) {
		cout << numero << endl;
	}
	
	string palabra = "hola";
	
	for(auto letra:palabra) {
		cout << letra << endl;
	}
	
	
	return 0;
}