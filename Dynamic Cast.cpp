#include <iostream>
using namespace std;

class Padre{
public:
    virtual void speak(){
        cout << "Padre!" << endl;
    }

};

class Hermano: public Padre{

};

class Hermana: public Padre {

};

int main() {

    Padre padre;
    Hermano hermano;

    Padre *pp = &hermano;

    Hermano *phoho = dynamic_cast<Hermano *>(pp);

    if (phoho == nullptr) {
        cout << "Casteo inválido" << endl;
    } else {
        cout << phoho << endl;
    }

    return 0;
}