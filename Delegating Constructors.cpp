#include <iostream>
using namespace std;

class Padre {
    int perros;
    string texto{"pepe"};

public:
    Padre(): Padre("pp") {
        cout << "nada de nada" << endl;
    }
    Padre(string texto) {
        cout << "constructor string Padre" << endl;
    }
};

class Hijo: public Padre{
public:
    Hijo()=default;
};

int main() {

    Padre padre("pp");
    Hijo hijo;
    return 0;
}