#include <iostream>
#include "Making Classes Iterable.h"
using namespace std;

int main(){
  ring<string> textring(3);

  textring.add("uno");
  textring.add("dos");
  textring.add("tres");
  textring.add("cuatro");
  textring.add("cinco");

  for(ring<string>::iterator it=textring.begin(); it !=textring.end(); it++) {
    cout << *it << endl;
  }

  for(string value: textring) {
      cout << value << endl;
  }

  return 0;

}