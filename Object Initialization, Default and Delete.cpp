#include <iostream>
using namespace std;

class Test {
	int id{3};
	string nombre{"pipo"};
public:
    Test() = default;
    Test(const Test &other) = default;
    Test &operator=(const Test &other) = default;
	Test(int id) : id(id) {
	}
	void print() {
		cout << id << ": " << nombre << endl;
	}
};

int main () {
	Test prueba;
	prueba.print();
	Test prueba1(34);
	prueba1.print();
	Test prueba2 = prueba1;
	prueba2.print();
	return 0;
}