#include <iostream>
#include <list>
using namespace std;

int main() {
	
	list <int> numeros;
	
	numeros.push_back(1);
	numeros.push_back(2);
	numeros.push_back(3);
	numeros.push_front(0);
	
	list<int>::iterator i = numeros.begin();
	i++;
	numeros.insert(i, 100);
	
	list<int>::iterator x = numeros.begin();
	x++;
	x = numeros.erase(x);
	
	for (list<int>::iterator i = numeros.begin(); i != numeros.end(); i++) {
		if (*i == 2) {
			numeros.insert(i, 200);
		}
		if (*i == 1) {
			i = numeros.erase(i);
		}
		else {
			i++;
		}
	}
	
	for (list<int>::iterator i = numeros.begin(); i != numeros.end(); i++) {
		cout << *i << endl;
	}
	
	
	return 0;
}